package mt.edu.mcast.bluetoothscanner;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter bluetoothAdapter;

    @BindView(R.id.rvDevices) RecyclerView rv;
    private DevicesAdapter devicesAdapter;
    private ArrayList<Device> deviceList = new ArrayList<>();

    @BindView(R.id.tvBluetoothStatus) TextView tvBluetoothStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));

        devicesAdapter = new DevicesAdapter(deviceList);
        rv.setAdapter(devicesAdapter);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Bluetooth not supported
        if (bluetoothAdapter == null) {
            new AlertDialog.Builder(this)
                .setTitle("Not compatible")
                .setMessage("Your phone does not support Bluetooth")
                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
            return;
        }

        if (!bluetoothAdapter.isEnabled()) {
            Snackbar snackbar = Snackbar.make(rv, "Don't forget to enable bluetooth before scan", Snackbar.LENGTH_LONG)
                .setAction("Enable", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bluetoothOn();
                    }
                });
            snackbar.show();
        }

        updateBluetoothStatus(bluetoothAdapter.isEnabled());

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mReceiver, filter);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                addDevice(device);
                return;
            }

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                boolean status = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                        == BluetoothAdapter.STATE_ON;
                updateBluetoothStatus(status);
                return;
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    @OnClick(R.id.btnOn)
    public void bluetoothOn(){
        if (bluetoothAdapter.isEnabled()) {
            toast("Bluetooth already on");
            return;
        }

        Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(turnOn, 0);
    }

    @OnClick(R.id.btnOff)
    public void off(){
        if (!bluetoothAdapter.isEnabled()) {
            toast("Bluetooth already off");
            return;
        }

        bluetoothAdapter.disable();
        toast("Turned bluetooth off");
    }

    @OnClick(R.id.btnVisible)
    public void visible(){
        Intent getVisible = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        startActivityForResult(getVisible, 0);
    }

    @OnClick(R.id.btnScan)
    public void scan(){
        // todo: check location permission!!!
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                Snackbar snackbar = Snackbar.make(rv, "Device scan requires location permission", Snackbar.LENGTH_LONG)
                    .setAction("Grant", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestLocationPermission();
                        }
                    });
                snackbar.show();
            } else {
                requestLocationPermission();
            }
            return;
        }

        if (!bluetoothAdapter.isEnabled()) {
            toast("Enable bluetooth before scan");
            return;
        }

        clearDevices();
        bluetoothAdapter.startDiscovery();
        toast("Looking for nearby devices...");
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
            new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION }, 1);
    }

    private void addDevice(BluetoothDevice device) {
        deviceList.add(new Device(device.getName(), device.getAddress()));
        devicesAdapter.notifyDataSetChanged();
    }

    private void clearDevices() {
        deviceList.clear();
        devicesAdapter.notifyDataSetChanged();
    }

    private void updateBluetoothStatus(boolean enabled) {
        if (enabled) {
            tvBluetoothStatus.setText("Bluetooth is on");
            tvBluetoothStatus.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
        } else {
            tvBluetoothStatus.setText("Bluetooth is off");
            tvBluetoothStatus.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
        }
    }

    private void toast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}
