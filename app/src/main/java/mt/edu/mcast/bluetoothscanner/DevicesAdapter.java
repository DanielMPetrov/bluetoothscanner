package mt.edu.mcast.bluetoothscanner;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.ViewHolder> {

    private ArrayList<Device> mDevices;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDeviceName) TextView deviceName;
        @BindView(R.id.tvDeviceAddress) TextView deviceAddress;

        public ViewHolder(View deviceItemView) {
            super(deviceItemView);
            ButterKnife.bind(this, deviceItemView);
        }
    }

    // Provide a suitable constructor (depends bluetoothOn the kind of dataset)
    public DevicesAdapter(ArrayList<Device> devices) {
        mDevices = devices;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public DevicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View deviceView = inflater.inflate(R.layout.device_item, parent, false);

        // ...

        return new ViewHolder(deviceView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // - get element from your dataset at this position
        Device device = mDevices.get(position);

        // - replace the contents of the view with that element
        holder.deviceName.setText(device.getName());
        holder.deviceAddress.setText(device.getAddress());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDevices.size();
    }
}
